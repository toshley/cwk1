package comp2931.cwk1;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Joshua Astle
 *         Date: November 2017
 *         Purpose: Unit tests for Date class.
 */
public class DateTest {

    private Date newMillennium;
    private Date endOfTime;
    private Date beginningOfTime;
    private Date todaysDate;

    /**
     * Test Fixtures
     */


    @Before
    public void setUp() {

        newMillennium = new Date(2000, 1, 1);
        beginningOfTime = new Date(0, 1, 1);
        endOfTime = new Date(9999, 12, 31);
        todaysDate = new Date();

    }

    /**
     * Check whether a string object is returned from the toString method.
     */
    @Test
    public void toStringIsString() {
        assertTrue(newMillennium.toString() instanceof String);
        assertTrue(beginningOfTime.toString() instanceof String);
        assertTrue(endOfTime.toString() instanceof String);

        assertFalse(!(newMillennium.toString() instanceof String));
        assertFalse(!(beginningOfTime.toString() instanceof String));
        assertFalse(!(endOfTime.toString() instanceof String));

    }

    /**
     * Check whether the new string object is null.
     */
    @Test
    public void isStringNull() {
        assertTrue(newMillennium.toString() != null);
        assertTrue(beginningOfTime.toString() != null);
        assertTrue(endOfTime.toString() != null);

        assertFalse(newMillennium.toString() == null);
        assertFalse(beginningOfTime.toString() == null);
        assertFalse(endOfTime.toString() == null);
    }

    /**
     * Check whether the converted String is formatted as expected.
     */
    @Test
    public void isStringExpected() {
        assertEquals("2000-01-01", newMillennium.toString());
        assertEquals("0000-01-01", beginningOfTime.toString());
        assertEquals("9999-12-31", endOfTime.toString());

        assertNotEquals("5000-01-01", newMillennium.toString());
        assertNotEquals("500-01-01", beginningOfTime.toString());
        assertNotEquals(beginningOfTime.toString(), endOfTime.toString());
    }

    /**
     * Check whether years below the lower bound can be input (just below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void yearsTooLow() {
        new Date(-1, 1, 1);

    }

    /**
     * Check whether years below the lower bound can be input
     * (further below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void yearsTooLowFurther() {
        new Date(-100, 1, 1);
    }

    /**
     * Check whether years below the lower bound can be input
     * (furthest below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void yearsTooLowFurthest() {
        new Date(-1000, 1, 1);
    }

    /**
     * Check whether years over the upper bound can be input (just above bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void yearsTooHigh() {
        new Date(10000, 1, 1);
    }

    /**
     * Check whether years over the upper bound can be input
     * (further above bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void yearsTooHighFurther() {
        new Date(25000, 1, 1);
    }

    /**
     * Check whether years over the upper bound can be input
     * (furthest above bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void yearsTooHighFurthest() {
        new Date(150000, 1, 1);
    }

    /**
     * Check whether months below the lower bound can be input
     * (just below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsTooLow() {
        new Date(10, -1, 1);
    }

    /**
     * Check whether months below the lower bound can be input
     * (further below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsTooLowFurther() {
        new Date(10, -50, 1);
    }

    /**
     * Check whether months below the lower bound can be input
     * (furthest below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsTooLowFurthest() {
        new Date(10, -200, 1);
    }

    /**
     * Check whether months over the upper bound can be input (just over bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsTooHigh() {
        new Date(10, 13, 1);
    }

    /**
     * Check whether months over the upper bound can be input
     * (further over bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsTooHighFurther() {
        new Date(10, 50, 1);
    }

    /**
     * Check whether months over the upper bound can be input (
     * furthest over bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void monthsTooHighFurthest() {
        new Date(10, 100, 1);
    }

    /**
     * Check whether days below the lower bound can be input (just below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysTooLow() {
        new Date(10, 11, -1);
    }

    /**
     * Check whether days below the lower bound can be input
     * (further below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysTooLowFurther() {
        new Date(10, 11, -50);
    }

    /**
     * Check whether days below the lower bound can be input
     * (furthest below bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysTooLowFurthest() {
        new Date(10, 11, -100);
    }

    /**
     * Check whether days above the upper bound can be input (just above bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysTooHigh() {
        new Date(10, 11, 32);
    }

    /**
     * Check whether days above the upper bound can be input
     * (further above bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysTooHighFurther() {
        new Date(10, 11, 60);
    }

    /**
     * Check whether days above the upper bound can be input
     * (furthest above bound).
     */
    @Test(expected = IllegalArgumentException.class)
    public void daysTooHighFurthest() {
        new Date(10, 11, 250);
    }

    /**
     * Check whether the number of days in February exceeds the allowed amount
     * (28).
     */
    @Test(expected = IllegalArgumentException.class)
    public void februaryDaysTooHigh() {
        new Date(10, 2, 31);
    }

    /**
     * Check whether a month with 30 days can have an input exceeding that amount
     */
    @Test(expected = IllegalArgumentException.class)
    public void aprilWith30DaysTooHigh() {

        new Date(10, 4, 31);
    }

    /**
     * Check whether the equals() method returns correctly
     */
    @Test
    public void equality() {
        assertTrue(newMillennium.equals(newMillennium));
        assertTrue(newMillennium.equals(new Date(2000, 1, 1)));
        assertFalse(newMillennium.equals(new Date(5000, 3, 1)));
        assertFalse(newMillennium.equals("2000-01-01"));
    }

    /**
     * Test whether getDayOfYear() returns the correct value
     */
    @Test
    public void getDayOfYearTest() {
        assertEquals(beginningOfTime.getDayOfYear(), 1);
        assertEquals(endOfTime.getDayOfYear(), 365);
        assertNotEquals(beginningOfTime.getDayOfYear(), endOfTime.getDayOfYear());
    }

    //TODO complete default Date constructor unit test
    @Test
    public void defaultConstructor() {


    }
}