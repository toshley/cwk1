// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;

import static java.lang.Integer.compare;

/**
 * @author Joshua Astle
 *         A Representation of a Date.
 */
public class Date {

    /**
     * Max & Min values for each class field.
     * <p>
     * NOTE: we assume that year 0 is allowed.
     */
    private static final int MAX_YEARS = 9999;
    private static final int MAX_MONTHS = 12;
    private static final int MAX_DAYS = 31;
    private static final int MIN_YEARS = 0;
    private static final int MIN_MONTHS = 1;
    private static final int MIN_DAYS = 1;

    /**
     * Each month is assigned its corresponding number.
     */
    private static final int JANUARY_NO = 1;
    private static final int FEBRUARY_NO = 2;
    private static final int MARCH_NO = 3;
    private static final int APRIL_NO = 4;
    private static final int MAY_NO = 5;
    private static final int JUNE_NO = 6;
    private static final int JULY_NO = 7;
    private static final int AUGUST_NO = 8;
    private static final int SEPTEMBER_NO = 9;
    private static final int OCTOBER_NO = 10;
    private static final int NOVEMBER_NO = 11;
    private static final int DECEMBER = 12;

    /**
     * Date fields.
     */
    private int year;
    private int month;
    private int day;

    /**
     * Default constructor, creates an object representing today's date
     * by taking an instance of the java.util.Calendar class.
     * <p>
     * NOTE: + 1 to the value returned by MONTH as the January index starts at 0.
     */
    public Date() {
        Calendar cal = Calendar.getInstance();

        this.setYear(cal.get(Calendar.YEAR));
        this.setMonth(cal.get(Calendar.MONTH));
        this.setDay(cal.get(Calendar.DAY_OF_MONTH));

    }

    /**
     * Creates a date using the given values for year, month and day.
     *
     * @param y Year
     * @param m Month
     * @param d Day
     */
    public Date(int y, int m, int d) {
        this.setYear(y);
        this.setMonth(m);
        this.setDay(d);
    }

    /**
     * Returns the year component of this date.
     *
     * @return Year
     */
    public int getYear() {
        return year;
    }

    /**
     * Returns the month component of this date.
     *
     * @return Month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns the day component of this date.
     *
     * @return Day
     */
    public int getDay() {
        return day;
    }

    /**
     * Returns the day of the year as an integer by finding the day of the year
     * a given month begins on, then adding the days.
     * <p>
     * NOTE: this function ignores leap years
     *
     * @return Day of the year as an Integer
     */
    public int getDayOfYear() {

        int dayOfYear = 0;

        switch (this.getMonth()) {
            case 1:
                dayOfYear = 0;
                break;
            case 2:
                dayOfYear = 31;
                break;
            case 3:
                dayOfYear = 59;
                break;
            case 4:
                dayOfYear = 90;
                break;
            case 5:
                dayOfYear = 120;
                break;
            case 6:
                dayOfYear = 151;
                break;
            case 7:
                dayOfYear = 181;
                break;
            case 8:
                dayOfYear = 212;
                break;
            case 9:
                dayOfYear = 243;
                break;
            case 10:
                dayOfYear = 273;
                break;
            case 11:
                dayOfYear = 304;
                break;
            case 12:
                dayOfYear = 334;
                break;
        }

        return dayOfYear += this.getDay();
    }

    /**
     * Sets the year component of this date.
     * <p>
     * NOTE: In this implementation I have assumed the year will not exceed 9999
     * due to formatting.
     *
     * @param year Year
     */
    public void setYear(int year) {
        if (year < MIN_YEARS || year > MAX_YEARS) {
            throw new IllegalArgumentException("Year must be between 0 - 9999");
        }
        this.year = year;
    }

    /**
     * Sets the month component of this date.
     *
     * @param month Month
     */
    public void setMonth(int month) {

        if (month < MIN_MONTHS || month > MAX_MONTHS) {
            throw new IllegalArgumentException("Month must be between 1 - 12");
        }
        this.month = month;
    }

    /**
     * Sets the day component of this date.
     *
     * @param day Day
     */
    public void setDay(int day) {

        if (day < MIN_DAYS || day > MAX_DAYS) {
            throw new IllegalArgumentException("days must be between 1-31");


        } else if (this.getMonth() == FEBRUARY_NO &&
                this.isLeapYear(this.year) == true) {
            if (day > 29) {
                throw new IllegalArgumentException("days must be between 1-31");
            }
        } else if (this.getMonth() == FEBRUARY_NO &&
                this.isLeapYear(this.year) == false) {
            if (day > 28) {
                throw new IllegalArgumentException("days must be between 1-31");
            }
        } else if (this.getMonth() == APRIL_NO ||
                this.getMonth() == JUNE_NO ||
                this.getMonth() == SEPTEMBER_NO ||
                this.getMonth() == NOVEMBER_NO && day > 30) {
            throw new IllegalArgumentException("The input month has only 30 days");
        } else {
            this.day = day;
        }
    }

    /**
     * Compares Date objects for equality of state
     *
     * @param other Object being compared
     * @return true if objects are of equal state || false if not
     */
    @Override
    public boolean equals(Object other) {

        // If object is compared with itself then return true
        if (this == other) {
            return true;
        }
        // Check if other is an instance of Date
        if (!(other instanceof Date)) {
            return false;
        }
        // typecast other to Date in order to compare data fields
        Date otherAsDate = (Date) other;

        // Compare data fields and return accordingly
        if (compare(this.getDay(), otherAsDate.getDay()) == 0 &&
                compare(this.getMonth(), otherAsDate.getMonth()) == 0 &&
                compare(this.getYear(), otherAsDate.getYear()) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks whether the given year is a leap year
     *
     * @param year Year
     * @return true if it is a leap year || false if not.
     */
    public boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Provides a string representation of this date.
     * <p>
     * ISO 8601 format is used (YYYY-MM-DD).
     *
     * @return Date as a string
     */
    @Override
    public String toString() {
        return String.format("%04d-%02d-%02d", year, month, day);
    }
}
